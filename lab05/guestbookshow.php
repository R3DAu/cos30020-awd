<?php

const __PAGE__ = 'Lab05 - Files &amp; Directories (Guestbook)';

//the relative path to the data directory to this script directory.
$data_dir = __DIR__ . '/../../data/';

//realpath returns the actual path, otherwise false if it doesn't exist.
//if the directory doesn't exist, let's create that.
if(!realpath($data_dir)) mkdir($data_dir, 0755, true);

//now we want to make sure that the data dir is now set to the actual path (just so permissions and syslinking isn't a thing.)
$data_dir = realpath($data_dir);
$file = "$data_dir/guestbook.txt";

//now we know the data directory exists, let's make sure our jobs file is in there.
if(!file_exists($file)) {
    //then we open, and instantly close the file for writing.
    $file_handle = fopen($file, 'w+') or die('Unable to open shop.txt for writing. Check file permissions!');
    fclose($file_handle);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- Bootstrap CSS -->
    <link href="/cos30020/s1793098/assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">

    <main>
        <div class="row">
            <div class="col-8 offset-2">
                <h2><?=__PAGE__?></h2>
                <?php
                if(filter_input(INPUT_GET, 'success') === "true")
                    echo '<div class="alert alert-success">Your entry has been saved!</div>';

                $handle = fopen($file, "r");
                echo "<table class='table'>
                        <thead class='thead-dark'>
                            <tr>
                                <th scope='col'>First Name</th>
                                <th scope='col'>Last Name</th>
                            </tr>
                        </thead>
                        <tbody>";

                while(!feof($handle)){
                    $data = fgets($handle);
                    if(!empty($data)){
                        $data = explode(",",$data);
                        echo "<tr><td>".$data[0]."</td><td>".$data[1]."</td></tr>";
                    }
                }
                echo "</tbody></table>";
                fclose($handle);
                ?>
                <a href="guestbookform.php" class="btn btn-secondary">Add new entry</a>
            </div>
        </div>
    </main>
</div>
</body>
</html>