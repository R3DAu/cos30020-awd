<?php const __PAGE__ = 'Lab05 - Files &amp; Directories'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- Bootstrap CSS -->
    <link href="/cos30020/s1793098/assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">

    <main>
        <div class="row">
            <div class="col-8 offset-2">
                <h1><?=__PAGE__?></h1>
                <form action="shoppingsave.php" method="post">
                    <div class="form-group mb-1">
                        <label for="item">Item</label>
                        <input class="form-control" type="text" name="item" id="item" placeholder="Item Name">
                    </div>
                    <div class="form-group mb-1">
                        <label for="qty">Quantity</label>
                        <input class="form-control" type="number" name="qty" id="qty" placeholder="1">
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary float-end" type="submit" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </main>
</div>
</body>
</html>