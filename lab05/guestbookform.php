<?php const __PAGE__ = 'Lab05 - Files &amp; Directories (Guestbook)'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- Bootstrap CSS -->
    <link href="/cos30020/s1793098/assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">

    <main>
        <div class="row">
            <div class="col-8 offset-2">
                <h2><?=__PAGE__?></h2>
                <form action="guestbooksave.php" method="post">
                    <?php
                        if(filter_input(INPUT_GET, 'err') === "save")
                            echo '<div class="alert alert-danger">Unable to save directly, please use the form below</div>';
                     ?>
                    <div class="form-group mb-1">
                        <label for="fname">First Name</label>
                        <input class="form-control" type="text" name="fname" id="fname" placeholder="Bill" required>
                    </div>
                    <div class="form-group mb-1">
                        <label for="lname">Last Name</label>
                        <input class="form-control" type="text" name="lname" id="lname" placeholder="Smith" required>
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary float-end" type="submit" value="Submit">
                    </div>
                </form>
                <a href="guestbookshow.php" class="btn btn-secondary">Show Guest Book</a>
            </div>
        </div>
    </main>
</div>
</body>
</html>