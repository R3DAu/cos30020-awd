<?php

//the relative path to the data directory to this script directory.
$data_dir = __DIR__ . '/../../data/';

//realpath returns the actual path, otherwise false if it doesn't exist.
//if the directory doesn't exist, let's create that.
if(!realpath($data_dir)) mkdir($data_dir, 0755, true);

//now we want to make sure that the data dir is now set to the actual path (just so permissions and syslinking isn't a thing.)
$data_dir = realpath($data_dir);
$file = "$data_dir/guestbook.txt";

//now we know the data directory exists, let's make sure our jobs file is in there.
if(!file_exists($file)) {
    //then we open, and instantly close the file for writing.
    $file_handle = fopen($file, 'w+') or die('Unable to open shop.txt for writing. Check file permissions!');
    fclose($file_handle);
}

if(filter_input(INPUT_SERVER, "REQUEST_METHOD") === "POST") {
    $first_name = trim(filter_input(INPUT_POST, "fname"));
    $last_name = trim(filter_input(INPUT_POST, "lname"));

    $err = $data = [];
    if(empty($first_name)) $err[] = "fn";
    if(empty($last_name)) $err[] = "ln";

    if(!empty($err))
        die("<h3>Unable to continue, all fields need to be filled out! Use the back button to continue.</h3>");

    //let's get ready to write the file.
    $handle = fopen($file, 'a');
    $data = "$first_name,$last_name" . PHP_EOL;
    fwrite($handle, $data);
    fclose($handle);

    //just redirect them back to the form...
    header("location: guestbookshow.php?success=true");
}else{
    //just redirect them back to the form...
    header("location: guestbookform.php?err=save");
}