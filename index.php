<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">


 <title>MTR - Landing Page</title>

 <!-- Google font -->
 <link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet">
 <link href="https://fonts.googleapis.com/css?family=Montserrat:900" rel="stylesheet">

 <!-- Custom stlylesheet -->
 <link type="text/css" rel="stylesheet" href="style.css" />

 <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
 <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
 <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body>
 <div id="mtrs">
  <div class="mtrs">
   <div class="mtrs-404">
    <h3>Home of Mitchell Reynolds</h3>
    <!--h1><span>M</span><span>T</span><span>R</span></h1-->
	<h1><span>s</span><span>1</span><span>7</span><span>9</span><span>3</span><span>0</span><span>9</span><span>8</span></h1>
   </div>
   <h2>Powered By Mercury</h2>
  </div> 
  <center>
	  <ul>
		<?php
			$dirs = array_diff(scandir(__DIR__), array('..', '.','.htaccess','assets'));
			foreach($dirs as $dir)
				if(is_dir($dir))
					echo "<li><a href='https://mercury.swin.edu.au/cos30020/s1793098/" . $dir . "'>".$dir."</a></li>";
		?>
	  </ul>
  </center>
 </div>
</body>

</html>
