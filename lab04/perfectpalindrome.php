<?php
/*
    Perfect Palindrome PHP
    Version 1
    Written by Mitchell Reynolds (1793098)
*/


//grab the string
$str = strtolower(trim(filter_input(INPUT_POST, "str")));

//check if the
if(filter_input(INPUT_SERVER, "REQUEST_METHOD") === "POST") {
    if (!empty($str)) {
        if(strcmp($str, strrev($str))===0)
            echo "<p>The word $str is a perfect palindrome";
        else
            echo "<p>The word $str is not a perfect palindrome";
    }
}
