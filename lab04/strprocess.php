<?php
/*
    String Process PHP
    Version 1
    Written by Mitchell Reynolds (1793098)
*/

const __PAGE__ = 'Lab 04 - Strings';

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?=__PAGE__?></title>
        <meta charset="utf-8">
        <meta name="description" content="Web development">
        <meta name="keywords" content="HTML, CSS, JavaScript">
        <meta name="author" content="Mitchell Reynolds">

        <!-- rebase the URLs to here... -->
        <base href="/cos30020/s1793098/">
        <!-- Bootstrap CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    </head>

    <body>
    <div class="col-lg-8 mx-auto p-3 py-md-5">
        <main>
            <h1><?=__PAGE__?></h1>

            <?php
                if(isset($_POST['str'])) {
                    $str = filter_input(INPUT_POST, "str");
                    $pattern = "/^[A-Za-z ]+$/";
                    if (preg_match($pattern, $str)) {
                        $ans = "";
                        $len = strlen($str);
                        for ($i = 0; $i < $len; $i++) {
                            $letter = substr($str, $i, 1);
                            if ((strpos("AEIOUaeiou", $letter)) === false) $ans .= $letter;
                        }
                        echo "<p>The word with no vowels is $ans </p>";
                    } else
                        echo "<p>Please enter a string containing only letters or a space";
                }else
                    echo "<p>Please enter a string from the input form";

            ?>
        </main>
    </div>
    </body>
</html>


