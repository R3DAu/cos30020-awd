<?php
/*
    Perfect Palindrome PHP
    Version 1
    Written by Mitchell Reynolds (1793098)
*/

/*
    Perfect Palindrome Form PHP
    Version 1
    Written by Mitchell Reynolds (1793098)
*/

const __PAGE__ = 'Lab 04 - Standard Palindrome'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?= __PAGE__ ?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- rebase the URLs to here... -->
    <!--base href="/cos30020/s1793098/"-->
    <!-- Bootstrap CSS -->
    <link href="/cos30020/s1793098/assets/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
        <div class="row">
            <div class="col-8 offset-2">
                <h1><?= __PAGE__ ?></h1>
                <?php
                //grab the string
                $o_str = trim(filter_input(INPUT_POST, "str"));
                $str = strtolower($o_str);

                //check if the
                if(filter_input(INPUT_SERVER, "REQUEST_METHOD") === "POST") {
                    $str = stripslashes($str);
                    $punctuation = ["!", ",", "\\", "/", "@", "#", "$", "%","^","&","*","(",")", ".","?",":",";","[","]","\"","'"," "];
                    $str = str_replace($punctuation, "",$str);

                    if (!empty($str)) {
                        if(strcmp($str, strrev($str))===0)
                            echo "<div class='alert alert-success'><p>The word $o_str is a perfect palindrome</p></div>";
                        else
                            echo "<div class='alert alert-danger'><p>The word $o_str is not a perfect palindrome</p></div>";
                    }
                }

                ?>
                <form action="standardpalindrome.php" method="post">
                    <div class="form-group mb-1">
                        <label for="str">Input a string to check:</label>
                        <input class="form-control" type="text" name="str" id="str">
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary float-end" type="submit" value="Submit"/>
                    </div>
                </form>
            </div>
        </div>
    </main>
</div>
</body>
</html>