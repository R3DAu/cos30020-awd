<?php
require_once('monsterclass.php');

//page name
const __PAGE__ = 'Lab10 - Monster OOPs';

//setup monsters.
$monster1 = new Monster(1, "red");
$monster2 = new Monster(3, "blue");
?>

<?php  ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- rebase the URLs to here... -->
    <base href="/cos30020/s1793098/">
    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
        <h1><?=__PAGE__?></h1>

        <p class="text-muted"><?=$monster1->describe()?></p>
        <p class="text-muted"><?=$monster2->describe()?></p>
    </main>
</div>
</body>
</html>
