<?php

class Monster
{
    private $num_of_eyes; //number of eyes
    private $colour; //the colour of the monster

    public function __construct($num, $col){
        $this->setNumOfEyes($num);
        $this->setColour($col);
    }

    //setter
    public function setNumOfEyes($num){
        $this->num_of_eyes = $num;
    }

    //getter
    public function getNumOfEyes(){
        return $this->num_of_eyes;
    }

    //setter
    public function setColour($col){
        $this->colour = $col;
    }

    //getter
    public function getColour(){
        return $this->colour;
    }

    //describe function
    public function describe(){
        return "The {$this->getColour()} monster has {$this->getNumOfEyes()} eyes.";
    }
}