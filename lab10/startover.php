<?php
//require the hitcounter class.
require_once('hitcounter.php');
//data dir
$data_dir = __DIR__ . '/../../data/lab10/';
$data_dir = realpath($data_dir);
$file = "$data_dir/mykeys.json";

//now we need to read the file - this just makes sure the data we wrote wasn't garbage.
$settings['database'] = file_get_contents($file);
//let's decode this - make sure we make it associative as well.
$settings['database'] = json_decode($settings['database'], true);

//get the hitcounter class setup.
$hitcounter = new HitCounter($settings);

//set a temporary counter.
$count = $hitcounter->startOver(1);
$hitcounter->closeConnection();
header("location: countvisits.php");
?>