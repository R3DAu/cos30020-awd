<?php
/*
    Written by Mitchell Reynolds (1793098)
*/

//templated page constant.
const __PAGE__ = 'Lab10 - Database Setup';

//let's import the database file.
require_once(__DIR__ . "/database.php");

//data dir
$data_dir = __DIR__ . '/../../data/lab10/';

//realpath returns the actual path, otherwise false if it doesn't exist.
//if the directory doesn't exist, let's create that.
if(!realpath($data_dir)) mkdir($data_dir, 0755, true);

//now we want to make sure that the data dir is now set to the actual path (just so permissions and syslinking isn't a thing.)
$data_dir = realpath($data_dir);
$file = "$data_dir/mykeys.json";

//now we know the data directory exists, let's make sure our file is in there.
if(!file_exists($file)) {
    //then we open, and instantly close the file for writing.
    $file_handle = fopen($file, 'w+') or die('Unable to open shop.txt for writing. Check file permissions!');
    fclose($file_handle);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- rebase the URLs to here... -->
    <base href="/cos30020/s1793098/">
    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
        <div class="row">
            <div class="col-8 offset-2">
                <h2 class="mb-3"><?=__PAGE__?></h2>

                <?php
                    if(filter_input(INPUT_SERVER, "REQUEST_METHOD") == "POST"){
                        //we will make this into an array to convert into JSON.
                        $database = [];
                        $database['host'] = filter_input(INPUT_POST, "host");
                        $database['port'] = filter_input(INPUT_POST, "port");
                        $database['dbnm'] = filter_input(INPUT_POST, "dbnm");
                        $database['tble'] = filter_input(INPUT_POST, "table");
                        $database['user'] = filter_input(INPUT_POST, "user");
                        $database['pass'] = filter_input(INPUT_POST, "pass");

                        //let's open the settings file for writing (we want to overwrite anything in this file...)
                        $fh = fopen($file, "w") or die("Unable to open $file for writing. Please check the permissions.");
                        //write the json encoded data to the file.
                        fwrite($fh, json_encode($database));
                        //closed the file.
                        fclose($fh);

                        //now we need to read the file - this just makes sure the data we wrote wasn't garbage.
                        $settings['database'] = file_get_contents($file);
                        //let's decode this - make sure we make it associative as well.
                        $settings['database'] = json_decode($settings['database'], true);

                        //let's make a new database class here.
                        $db = new database($settings);

                        //let's connect.
                        $db->connect();

                        //now let's check for the hitcount table.
                        if(!$db->table_exists($settings['database']['tble'])){
                            //let's create some columns....
                            $columns = array(
                                "id SMALLINT NOT NULL",
                                "hits SMALLINT NOT NULL",
                                "PRIMARY KEY (id)"
                            );

                            //let's make the table.
                            $db->table_create($settings['database']['tble'], $columns);

                            //now we are going to add just one counter object for now.
                            $stmt = $db->con->query("INSERT INTO {$settings['database']['tble']}(id, hits) VALUES (1,0)");
                        }

                        echo "<div class='alert alert-success p-0 m-o mb-1 text-center'>Successfully wrote file and created database...</div>";
                    }
                ?>

                <form action="lab10/setup.php" method="post">
                    <div class="form-group mb-1">
                        <label for="host">Host</label>
                        <input class="form-control" type="text" id="host" name="host" value="localhost" required/>
                    </div>
                    <div class="form-group mb-1">
                        <label for="port">Port</label>
                        <input class="form-control" type="text" pattern="^\d{1,5}$" id="port" name="port" value="3306" required/>
                    </div>
                    <div class="form-group mb-1">
                        <label for="dbnm">Database Name</label>
                        <input class="form-control" type="text" id="dbnm" name="dbnm" placeholder="sxxxx_db" required/>
                    </div>
                    <div class="form-group mb-1">
                        <label for="table">Table Name</label>
                        <input class="form-control" type="text" id="table" name="table" placeholder="table" required/>
                    </div>
                    <div class="form-group mb-1">
                        <label for="user">Username</label>
                        <input class="form-control" type="text" id="user" name="user" placeholder="root" required/>
                    </div>
                    <div class="form-group mb-1">
                        <label for="pass">Password</label>
                        <input class="form-control" type="password" id="pass" name="pass" placeholder="Password" required/>
                    </div>

                    <div class="row">
                        <div class="col-2 mt-2 ms-3 me-0 p-0">
                            <input class="btn btn-primary" style="width:100%" type="submit" value="Submit">
                        </div>
                        <div class="col-2 mt-2 ms-1 p-0">
                            <input class="btn btn-danger" style="width:100%" type="reset" value="Reset">
                        </div>
                    </div>
                </form>
            </div>
    </main>
</div>
</body>
</html>