<?php
    const __PAGE__ = 'Lab10 - Count Visits';

    //require the hitcounter class.
    require_once('hitcounter.php');
    //data dir
    $data_dir = __DIR__ . '/../../data/lab10/';
    $data_dir = realpath($data_dir);
    $file = "$data_dir/mykeys.json";

    //now we need to read the file - this just makes sure the data we wrote wasn't garbage.
    $settings['database'] = file_get_contents($file);
    //let's decode this - make sure we make it associative as well.
    $settings['database'] = json_decode($settings['database'], true);

    //get the hitcounter class setup.
    $hitcounter = new HitCounter($settings);

    //set a temporary counter.
    $count = $hitcounter->getHits(1)['hits'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- rebase the URLs to here... -->
    <base href="/cos30020/s1793098/">
    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
        <h1><?=__PAGE__?></h1>
        <p class="text-muted">This page has received <?=$count?> hits.</p>
        <p class="text-muted"><a href="lab10/startover.php">Start Over</a></p>
    </main>
</div>
</body>
</html>

<?php
    //increment the counter.
    $count++;
    //save the count.
    $hitcounter->setHits(1,$count);
    //close the connection.
    $hitcounter->closeConnection();
?>