<?php
require_once(__DIR__ . '/database.php');

class HitCounter
{
    //initialize some settings
    private $database = null;
    private $settings = null;

    public function __construct($settings){
        $this->settings = $settings;
        $this->database = new database($settings);
        $this->database->connect();
    }

    public function closeConnection(){
        $this->database->disconnect();
    }

    public function getHits($id){
        $sql = "SELECT * FROM {$this->settings['database']['tble']} WHERE id = ?";
        $stmt = $this->database->con->prepare($sql);
        $stmt->execute(array($id));
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function setHits($id, $hits){
        $sql = "UPDATE {$this->settings['database']['tble']} SET hits=? WHERE id = ?";
        $stmt = $this->database->con->prepare($sql);
        $stmt->execute(array($hits,$id));
        return true;
    }

    public function startOver($id){
        $sql = "UPDATE {$this->settings['database']['tble']} SET hits=0 WHERE id = ?";
        $stmt = $this->database->con->prepare($sql);
        $stmt->execute(array($id));
        return true;
    }
}