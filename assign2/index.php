<?php
    //page name constant
    const __PAGE__ = 'Assignment 3 - Home Page';

    //import the database class and settings
    require_once(__DIR__ . '/database.php');
    require_once(__DIR__ . '/settings.inc.php');

    //create a new database class
    $db = new Database($settings);

    //now connect to the database.
    $db->connect();

    if(!$db->table_exists("friends")) {
        //let's create the first table
        $friends_columns = array(
            "friend_id INT AUTO_INCREMENT NOT NULL",
            "friend_email VARCHAR(50) NOT NULL",
            "password VARCHAR(20) NOT NULL",
            "profile_name VARCHAR(30) NOT NULL",
            "date_started DATE NOT NULL",
            "num_of_friends INT UNSIGNED",
            "PRIMARY KEY (friend_id)"
        );

        $db->table_create("friends", $friends_columns);

        //let's populate this table.
        //let's start a transaction as we are dealing with a lot of COMMITS (turns off AUTOCOMMIT)
        $db->con->beginTransaction();

        try {
            $sql = "INSERT INTO friends (friend_email, password, profile_name, date_started, num_of_friends) VALUES (?, ?, ?, ?, ?)";
            $stmt = $db->con->prepare($sql);

            //first friend.
            $stmt->execute(array("joe.blogs@gmail.com", "1AmJoeTheGreat", "JoeBlogs", "2010-08-19", 2));

            //second friend.
            $stmt->execute(array("jack.smith@gmail.com", "1AmSmithThePowerful", "JackSmith", "2011-04-29", 2));

            //third friend.
            $stmt->execute(array("barry.smith@gmail.com", "BarryTheCheerful1", "BarrySmith", "2008-12-02", 1));

            //fourth friend.
            $stmt->execute(array("cherri.goldburn@hotmail.com", "CherriHasItAll30", "CherriGoldburn", "1995-11-21", 3));

            //fifth friend.
            $stmt->execute(array("leanne.shear@hotmail.com", "92HotWheelsLover", "LeanneShear", "2001-05-17", 0));

            //sixth friend.
            $stmt->execute(array("jaquline.moare@hotmail.com", "WhereArtThouCindyLou43)", "JaqulineMoare", "2007-05-19", 0));

            //seventh friend.
            $stmt->execute(array("mary.silverfield@hotmail.com", "CatchMeGreenKeeper2022", "MarySilverfield", "2022-08-01", 2));

            //eighth friend
            $stmt->execute(array("roseanna.marina@yahoo.com", "Power2ThePeople45", "RoseannaMarina", "2019-06-05", 6));

            //ninth friend.
            $stmt->execute(array("william.shatner@yahoo.com", "BeamMeUpScotty404", "BillShatner", "2012-10-08", 2));

            //tenth friend.
            $stmt->execute(array("frank.drew@gmail.com", "TheOneTheOnlyThe9Yards", "FrankDrew", "2015-03-11", 2));

            //commit the transactions
            $db->con->commit();
        }catch(Exception $e){
            //rollback any of the entries.
            $db->con->rollback();
            die("An error has occurred: $e");
        }
    }

    if(!$db->table_exists("myfriends")) {
        $myfriends_columns = array(
            "friend_id1 INT NOT NULL",
            "friend_id2 INT NOT NULL"
        );

        $db->table_create("myfriends", $myfriends_columns);

        //let's populate this table.
        //let's start a transaction as we are dealing with a lot of COMMITS (turns off AUTOCOMMIT)
        $db->con->beginTransaction();

        try {
            $sql = "INSERT INTO myfriends (friend_id1, friend_id2) VALUES (?, ?)";
            $stmt = $db->con->prepare($sql);

            $stmt->execute(array(1,2));
            $stmt->execute(array(1,3));
            $stmt->execute(array(2,1));
            $stmt->execute(array(2,10));
            $stmt->execute(array(3,1));
            $stmt->execute(array(4,8));
            $stmt->execute(array(4,9));
            $stmt->execute(array(4,10));
            $stmt->execute(array(7,8));
            $stmt->execute(array(8,4));
            $stmt->execute(array(8,5));
            $stmt->execute(array(8,6));
            $stmt->execute(array(8,7));
            $stmt->execute(array(8,9));
            $stmt->execute(array(9,4));
            $stmt->execute(array(9,7));
            $stmt->execute(array(10,2));
            $stmt->execute(array(10,4));

            //commit the transactions
            $db->con->commit();
        }catch(Exception $e){
            //rollback any of the entries.
            $db->con->rollback();
            die("An error has occurred: $e");
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <link href="assets/css/index.css" rel="stylesheet" />
    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
        <div class="row">
            <h1 class="mb-3"><?=__PAGE__?></h1>
            <hr />
            <p class="mb-5">Page by Mitchell Reynolds (1793098) - 1793098@student.swin.edu.au</p>

            <p>I declare that this assignment is my individual work. I have not worked  collaboratively nor have I copied from any other student’s work or from any other source.</p>
        </div>

        <div class="row">
            <div class="col-6">
                <ul>
                    <li><a href="signup.php">Sign Up</a></li>
                    <li><a href="login.php">Log In</a></li>
                </ul>
            </div>
            <div class="col-6">
                <ul>
                    <li><a href="about.php">About this assignment</a></li>
                </ul>
            </div>
        </div>


    </main>
</div>
</body>
</html>