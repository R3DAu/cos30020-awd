<?php

class Database
{
    public $con = null;
    private $constring = null;
    private $user = null;
    private $pass = null;

    public function __construct(array $settings){
        $this->constring = "mysql:dbname=" . $settings['database']['dbnm'] . ";host=" . $settings['database']['host'] . ";port=" . $settings['database']['port'];
        $this->user = $settings['database']['user'];
        $this->pass = $settings['database']['pass'];

        if(empty($this->user) || empty($this->pass)) throw new Exception("The database username and/or password cannot be empty. Please fix these in the settings.php file");
    }

    public function connect(){
        try {
            $this->con =  new PDO($this->constring, $this->user, $this->pass);
            $this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $pdoE){
            throw new Exception("Unable to proceed - PDO exception error: " . $pdoE->getMessage());
        }catch(Exception $e){
            throw new Exception("Unable to proceed - PHP exception error: " . $e->getMessage());
        }
    }

    public function disconnect(){
        try {
            //in PDO, you destroy the PDO object which in tern closes the connection automatically.
            $this->con = null;
        }catch(PDOException $pdoE){
            throw new Exception("Unable to proceed - PDO exception error: " . $pdoE->getMessage());
        }catch(Exception $e){
            throw new Exception("Unable to proceed - PHP exception error: " . $e->getMessage());
        }
    }

    public function table_exists($tableName){
        try {
            $result =  $this->con->query("SELECT 1 FROM {$tableName}");
        }catch(PDOException $pdoE){
            return FALSE;
        }catch(Exception $e){
            return FALSE;
        }

        return $result !== FALSE;
    }

    public function table_create($name, array $columns){
        try {
            $sql = "CREATE TABLE IF NOT EXISTS $name (" . implode(',', $columns) . ") ENGINE=InnoDB DEFAULT CHARSET=latin1";
            $this->con->exec($sql);
        }catch(PDOException $pdoE){
            throw new Exception("Unable to proceed - PDO exception error: " . $pdoE->getMessage());
        }catch(Exception $e){
            throw new Exception("Unable to proceed - PHP exception error: " . $e->getMessage());
        }
    }
}