<?php
//start the session
session_start();

//page name constant
const __PAGE__ = 'Assignment 3 - My Friend System';

//import the database class and settings
require_once(__DIR__ . '/database.php');
require_once(__DIR__ . '/settings.inc.php');

//create a new database class
$db = new Database($settings);

//now connect to the database.
$db->connect();

//let's check if they are signed in...
if(!isset($_SESSION['login'])) header("location: login.php");

//let's also make sure the user exists....
$stmt = $db->con->prepare("SELECT * FROM friends WHERE friend_email = ?");
$stmt->execute(array(filter_var($_SESSION['login'])));
$users = $stmt->fetchAll(PDO::FETCH_ASSOC);

//that means someone had forged the SESSION...
if(count($users) <= 0){
    unset($_SESSION['login']);
    session_destroy();
    header("location: login.php");
}

//there should only be one user...
$user = array_shift($users);

if(isset($_GET['removefriend'])){
    //this is where we can remove the friend, then redirect the page.

    //check if the link exists first...
    $stmt = $db->con->prepare("SELECT * FROM myfriends WHERE friend_id1 = ? AND friend_id2 = ?");
    $stmt->execute(array($user['friend_id'],filter_input(INPUT_GET, 'removefriend')));
    $check = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if(count($check) > 0) {
        //remove both links...
        $stmt = $db->con->prepare("DELETE FROM myfriends WHERE friend_id1 = ? AND friend_id2 = ?");
        $stmt->execute(array($user['friend_id'],filter_input(INPUT_GET, 'removefriend')));
        $stmt->execute(array(filter_input(INPUT_GET, 'removefriend'), $user['friend_id']));

        //update the friend counters...
        $stmt = $db->con->prepare("UPDATE friends SET num_of_friends = num_of_friends - 1 WHERE friend_id = ?");
        $stmt->execute(array($user['friend_id']));
        $stmt->execute(array(filter_input(INPUT_GET, 'removefriend')));
    }

    //now redirect.
    header("location: friendlist.php");
}

//for pagination we need to get a result count
$stmt = $db->con->prepare("SELECT COUNT(*) c FROM myfriends LEFT JOIN friends ON friends.friend_id=myfriends.friend_id2 WHERE myfriends.friend_id1 = ?");
$stmt->execute(array($user['friend_id']));
//a bit risky doing it like this as the return could be a null. therefor we could get a null return and we can't reference a null return.
$rescount = $stmt->fetchAll(PDO::FETCH_CLASS)[0]->c or ($rescount = 0);
$maxpages = ceil($rescount / 5) - 1;

//set a default
$page = 0;

//if we have a page set...
if(isset($_GET['page'])){
    $page = filter_input(INPUT_GET, 'page',FILTER_SANITIZE_NUMBER_INT);

    //fix it so we don't get any issues.
    if($page > $maxpages) $page = $maxpages;
    if($page < 0) $page = 0;

    $stmt = $db->con->prepare("SELECT * FROM myfriends LEFT JOIN friends ON friends.friend_id=myfriends.friend_id2 WHERE myfriends.friend_id1 = ? ORDER BY friends.profile_name ASC LIMIT 5 OFFSET " . ($page * 5));
    $stmt->execute(array($user['friend_id']));

    $friends = $stmt->fetchAll(PDO::FETCH_ASSOC);
}else{
    $stmt = $db->con->prepare("SELECT * FROM myfriends LEFT JOIN friends ON friends.friend_id=myfriends.friend_id2 WHERE myfriends.friend_id1 = ? ORDER BY friends.profile_name ASC LIMIT 5");
    $stmt->execute(array($user['friend_id']));
    $friends = $stmt->fetchAll(PDO::FETCH_ASSOC);
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <link href="assets/css/index.css" rel="stylesheet" />
    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
        <center class="row">
            <div class="col-12 text-center">
                <h1 class="mb-3"><?=__PAGE__?></h1>
                <h3 class="m-0"><?=$user['profile_name']?> Friend List Page</h3>
                <h3 class="mb-3">Total number of friends is <?=$user['num_of_friends']?></h3>
            </div>

            <div class="col-8 offset-2">
                <table class="table table-dark table-striped text-center">
                    <tbody>
                        <!-- I used an anchor tag because I wasn't sure if we could use javascript - otherwise I would have used onclick="" -->
                        <?php
                            foreach($friends as $friend){
                                echo "<tr><td>{$friend['profile_name']}</td><td><a href='friendlist.php?removefriend={$friend['friend_id']}'><button>Unfriend</button></a></td></tr>";
                            }
                        ?>

                    </tbody>
                </table>
            </div>
            <div class="d-flex justify-content-center mb-4">
                <nav class="text-center" aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item"><a class="page-link <?=($page <= 0)?"disabled":""?>" href="?page=<?=$page-1?>">Previous</a></li>
                        <?php
                            for($i=0;$i <= $maxpages;$i++) {
                                echo '<li class="page-item"><a class="page-link ' . (($page == $i) ? "active" : "") . '" href="?page=' . $i . '">'. ($i + 1) .'</a></li>';
                            }
                        ?>
                        <li class="page-item"><a class="page-link <?=($page >= ($maxpages))?"disabled":""?>" href="?page=<?=$page+1?>">Next</a></li>
                    </ul>
                </nav>
            </div>
            <p class="text-center text-muted"><a href="friendadd.php">Add Friends</a>&nbsp;&nbsp;&nbsp;<a href="logout.php">Log Out</a></p>

        </div>
    </main>
</div>
</body>
</html>
