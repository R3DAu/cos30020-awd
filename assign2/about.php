<?php
/*
    Post Job Form PHP
    Version 1
    Written by Mitchell Reynolds (1793098)
*/

//templated page constant.
const __PAGE__ = 'Assignment 3 - About Page';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
        <div class="row">
            <div class="col-8 offset-2">
                <h2 class="mb-5"><?=__PAGE__?></h2>

                <h3 class="mb-1">What tasks have you not attempted or completed?</h3>
                <p class="lead">I have only one task that I haven't completed in the unit so far which is Task 2 (part 3) - I had completely forgotten this part.</p>

                <h3 class="mb-1">What special features have you done, or attempted, in creating the site that we should know about?</h3>
                <ul class="mb-5">
                    <li>I have created this site with the Bootstrap 5 CSS/SASS framework</li>
                    <li>Some interesting features is that when you add a friend, You also get added to their friend list too.</li>
                    <li>I have implemented some new-age functionality in regard to inline-sanitization of form inputs (use of `filter_input()`)</li>
                    <li>(personal) I have also been using JetBrains' PHPStorm IDE</li>
                    <li>All of my labs and assignments are in <a target="_blank" href="https://gitlab.com/R3DAu/cos30020-awd">GitLab</a></li>
                </ul>

                <h3 class="mb-1">Which parts did you have trouble with?</h3>
                <ul class="mb-5">
                    <li>Mostly with the array comparing with the database (it was tricky but managed it) in task 5 (this assignment)</li>
                </ul>

                <h3 class="mb-1">What would you like to do better next time?</h3>
                <ul class="mb-5">
                    <li>More or less, being able to create my templates better. Maybe add a setup page for the assignment.</li>
                </ul>

                <h3 class="mb-1">What additional features did you add to the assignment? (if any</h3>
                <ul class="mb-5">
                    <li>Duplicate of the above?</li>
                    <li>Utilized PDO drivers instead of the much older MySQLi drivers.</li>
                    <li>Made a class for the database (reduces code reuse/code pollution)</li>
                    <li>Added Pagination (as the optional extra)</li>
                </ul>

                <h3 class="mb-3 mt-2">A screen shot of a discussion response that answered someone’s thread in the unit’s
                    discussion board for Assignment 3?
                </h3>
                <div class="row">
                    <div class="col-8 offset-2">
                        <img class="img-fluid " src="assets/img/HelpingDiscord.png" alt="discussion-board-topic">
                    </div>
                </div>
                <p class="lead">Unfortunately, I was too late to answer any of the questions however, I did assist someone on my discord with their week 11 tasks and have been a regular to the online tutorials</p>

                <div class="row">
                    <div class="col-12 mt-4">
                        <p class="text-muted text-center"><a href="index.php">Return to home page</a>&nbsp;&nbsp;<a href="friendadd.php">Add Friends</a>&nbsp;&nbsp;<a href="friendlist.php">List Friends</a></p>
                    </div>
                </div>
            </div>
    </main>
</div>
</body>
</html>