<?php
//start the session
session_start();

//page name constant
const __PAGE__ = 'Assignment 3 - My Friend System';

//import the database class and settings
require_once(__DIR__ . '/database.php');
require_once(__DIR__ . '/settings.inc.php');

//create a new database class
$db = new Database($settings);

//now connect to the database.
$db->connect();

//set some basic information for us...
$error['any']   = false;
$error['email'] = false;
$error['name']  = false;
$error['pass']  = false;
$error['cpss']  = false;
$error['db']    = false;

$is_post = (filter_input(INPUT_SERVER, "REQUEST_METHOD") == "POST");

//let's check post values.
if($is_post){
    //Let's reset the values as we go along.
    $error['any']   = false;
    $error['email'] = false;
    $error['name']  = false;
    $error['pass']  = false;
    $error['cpss']  = false;
    $error['db']    = false;

    //let's start with pulling in the values to manipulate.
    $email      = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL);
    $prof_name  = filter_input(INPUT_POST, "profile_name");
    $pass       = filter_input(INPUT_POST, "password");
    $conf_pass  = filter_input(INPUT_POST, "confirm_password");

    //set both error any and error email to true if we can't validate the email correctly.
    if(!filter_var($email,FILTER_VALIDATE_EMAIL)) $error['any'] = $error['email'] = true;

    //we need to also check the table to make sure this username is unique.
    $stmt = $db->con->prepare("SELECT * FROM friends WHERE friend_email = ?");
    $stmt->execute(array($email));
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

    //then there is an account with that email address.
    if(count($results) > 0) $error['any'] = $error['email'] = true;

    //check the name.
    $match = preg_match('/^[A-Za-z]{1,30}$/',$prof_name);
    if(!$match) $error['any'] = $error['name'] = true;

    //finally, check the passwords.
    $match = preg_match('/^[A-Za-z0-9]{1,20}$/',$pass);
    if(!$match) $error['any'] = $error['pass'] = true;

    //check if the passwords match.
    if($pass !== $conf_pass) $error['any'] = $error['cpss'] = true;

    //if there are no errors, let's add this to the database.
    if(!$error['any']){
        //so then we are okay to import this into the database.
        try{
            $sql = "INSERT INTO friends (friend_email, password, profile_name, date_started, num_of_friends) VALUES (?,?,?,?,0)";
            $stmt = $db->con->prepare($sql);
            $stmt->execute(array($email, $pass, $prof_name, date('Y-m-d')));

            //if we got up to this point then we can log in...
            $_SESSION['login'] = $email;

            //send over to adding friends page.
            header("location: friendadd.php");

            return; //don't keep going...
        }catch(Exception $e){
            //if we got an Exception, handle it here...
            $error['any'] = $error['db'] = true;
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <link href="assets/css/index.css" rel="stylesheet" />
    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
        <div class="row">
            <div class="col-12 text-center">
                <h1 class="mb-3"><?=__PAGE__?></h1>
                <h3 class="mb-3">Registration Page</h3>
            </div>

            <div class="col-8 offset-2">
                <?=($is_post && $error['db'])?"<div class='alert alert-danger'>An error occurred importing the profile into the database, Please try again later.</div>":""?>
                <form action="signup.php" method="post">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input class="form-control <?=($is_post&&$error['email'])?'is-invalid':(($is_post)?'is-valid':'')?>" <?=($is_post)?"value='$email'":""?> type="email" id="email" name="email" placeholder="jane.doe@example.com" required>
                        <?=($is_post&&$error['email'])?'<div class="invalid-feedback">Email is invalid or has been registered already.</div>':''?>
                    </div>
                    <div class="form-group">
                        <label for="profile_name">Profile Name</label>
                        <input class="form-control <?=($is_post&&$error['name'])?'is-invalid':(($is_post)?'is-valid':'')?>" <?=($is_post)?"value='$prof_name'":""?> type="text" id="profile_name" name="profile_name" placeholder="JaneDoe" required>
                        <?=($is_post&&$error['name'])?'<div class="invalid-feedback">The profile name is invalid. It can only contain letters (no spaces or symbols)</div>':''?>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input class="form-control <?=($is_post&&$error['pass'])?'is-invalid':(($is_post)?'is-valid':'')?>" type="password" id="password" name="password" placeholder="Password" required>
                        <?=($is_post&&$error['pass'])?'<div class="invalid-feedback">The password is invalid. It can only contain Alphanumeric characters (no spaces or symbols)</div>':''?>
                    </div>
                    <div class="form-group">
                        <label for="confirm_password">Confirm Password</label>
                        <input class="form-control <?=($is_post&&$error['cpss'])?'is-invalid':(($is_post)?'is-valid':'')?>" type="password" id="confirm_password" name="confirm_password" placeholder="Confirm Password" required>
                        <?=($is_post&&$error['pass'])?'<div class="invalid-feedback">The passwords don\'t match.</div>':''?>
                    </div>
                    <div class="form-group justify-content-center text-center mx-0 p-0 my-4">
                        <input type="submit" class="btn btn-info col-3" value="Register"/>
                        <input type="reset"  class="btn btn-secondary col-3" value="Clear" />
                    </div>
                    <p class="text-center text-muted">All fields are required. <br/> <br/> <a href="index.php">Home</a></p>
                </form>

            </div>
        </div>


    </main>
</div>
</body>
</html>