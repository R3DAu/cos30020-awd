<?php
//start the session
session_start();

//page name constant
const __PAGE__ = 'Assignment 3 - My Friend System';

//import the database class and settings
require_once(__DIR__ . '/database.php');
require_once(__DIR__ . '/settings.inc.php');

//create a new database class
$db = new Database($settings);

//now connect to the database.
$db->connect();

//set some basic information for us...
$error['any']   = false;
$error['email'] = false;
$error['pass']  = false;
$error['db']    = false;

$is_post = (filter_input(INPUT_SERVER, "REQUEST_METHOD") == "POST");

//let's check post values.
if($is_post){
    //Let's reset the values as we go along.
    $error['any']   = false;
    $error['email'] = false;
    $error['pass']  = false;
    $error['db']    = false;

    //let's start with pulling in the values to manipulate.
    $email      = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL);
    $pass       = filter_input(INPUT_POST, "password");

    //set both error any and error email to true if we can't validate the email correctly.
    if(!filter_var($email,FILTER_VALIDATE_EMAIL)) $error['any'] = $error['email'] = true;

    //we need to also check the table to make sure this username is unique.
    $stmt = $db->con->prepare("SELECT * FROM friends WHERE friend_email = ?");
    $stmt->execute(array($email));
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

    //then there is an account with that email address.
    if(count($results) <= 0)
        $error['any'] = $error['email'] = true;
    else{
        //we have a result.
        //check for password...
        if($pass === $results[0]["password"]){
            $_SESSION['login'] = $email;
            header('location: friendlist.php');
        }else{
            $error['any'] = $error['pass'] = true;
        }
    }

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <link href="assets/css/index.css" rel="stylesheet" />
    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
        <div class="row">
            <div class="col-12 text-center">
                <h1 class="mb-3"><?=__PAGE__?></h1>
                <h3 class="mb-3">Log in Page</h3>
            </div>

            <div class="col-4 offset-4">
                <?=($error['any'] && ($error['email'] || $error['pass']))?'<div class="alert alert-danger text-center">The email or password is incorrect.</div>':''?>
                <form action="login.php" method="post">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input class="form-control <?=($is_post&&$error['any'])?'is-invalid':(($is_post)?'is-valid':'')?>" <?=($is_post && !empty($email))?"value='$email'":""?> type="email" id="email" name="email" placeholder="jane.doe@example.com" required>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input class="form-control <?=($is_post&&$error['any'])?'is-invalid':(($is_post)?'is-valid':'')?>" type="password" id="password" name="password" placeholder="Password" required>
                    </div>
                    <div class="form-group justify-content-center text-center mx-0 p-0 my-4">
                        <input type="submit" class="btn btn-info col-4" value="Login"/>
                        <input type="reset"  class="btn btn-secondary col-4" value="Clear" />
                    </div>
                    <p class="text-center text-muted"> <a href="index.php">Home</a></p>
                </form>

            </div>
        </div>
    </main>
</div>
</body>
</html>