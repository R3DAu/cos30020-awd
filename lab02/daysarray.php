<!DOCTYPE html>
<html lang="en">
<head>
    <title>Days Array</title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">
</head>
<body>
<h1>Days array</h1>
<?php
//predefined array.
$days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
echo "The days of the week in English are: " . implode(', ', $days);

//just add some line breaks to show the difference.
echo "<br/>";
echo "<br/>";

//French days of the week.
$days = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
echo "The days of the week in French are: " . implode(', ', $days);
?>
</body>
</html>