<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Module 02</title>
        <meta charset="utf-8">
        <meta name="description" content="Web development">
        <meta name="keywords" content="HTML, CSS, JavaScript">
        <meta name="author" content="Mitchell Reynolds">
    </head>
    <body>
    <h1>Average Score Calculator</h1>
    <?php
        //predefined array.
        $marks = [85, 85, 95];
        $marks[1] = 90;

        $avg = (array_sum($marks) / count($marks));

        echo  "The average score is " . $avg . ". You " . (($avg > 50) ? "passed" : "failed"). ".";
    ?>
    </body>
</html>