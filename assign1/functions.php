<?php
/*
    Functions PHP
    Version 1
    Written by Mitchell Reynolds (1793098)
*/

/**
 * createDir creates the directory specified. Returns true if successful.
 * @param $dir
 * @return bool
 */
function createDir($dir)
{
    //checks if the path actually exists.
    if(!realpath($dir)){
        //catch exceptions.
        try{
            //make directory with 0755 permissions.
            mkdir($dir, 0755, true);
            return true;
        }catch(Exception $e){
            //handle exception.
            die("Unable to create directory: $dir, check the permissions. Error: " . $e->getMessage());
        }
    }else
        //return false as the file already exists.
        return false;
}

/**
 * checkDir will check if the dir exists, will then return the real path if successful.
 * @param $dir
 * @return false|string
 */
function checkDir($dir)
{
    //send to createDir function
    createDir($dir);
    //return the realpath.
    return realpath($dir);
}

/**
 * createFile will check if the file exists and creates it if it doesn't. Returns true if successful.
 * @param $file
 * @return bool|void
 */
function createFile($file)
{
    if(!file_exists($file)) {
        //then we open, and instantly close the file for writing.
        $file_handle = fopen($file, 'w+') or die("Unable to open $file for writing. Check file permissions!");
        fclose($file_handle);
        chmod($file,0755);
        return true;
    }
    return false;
}

/**
 * readTabDelimitedFile - Reads a tab delimited file then return as an array
 * @param $file
 * @return array|void
 */
function readTabDelimitedFile($file)
{
    $contents = [];
    $file_handle = fopen($file, 'r') or die("Unable to open $file for reading. Check file permissions!");
    while(!feof($file_handle)) {
        //get the line (or the string)
        $str = fgets($file_handle);
        //check for empty lines...
        if(!empty($str)) {
            //if not empty, write to the contents array. Exploding the string by tabs (for fields).
            $arr = explode("\t", $str);
            //let's organise this array by the ID at the start.
            $contents[$arr[0]] = $arr;
        }
    }
    //make sure to destroy the resource
    fclose($file_handle);

    //return the contents.
    return $contents;
}

/**
 * writeTabDelimitedFile writes the array data specified to the file specified in tab delimited format.
 * @param $file
 * @param $data
 * @return void
 */
function writeTabDelimitedFile($file, $data){
    $file_handle = fopen($file, 'a') or die("Unable to open $file for writing. Check file permissions!");
    //convert the array to a string, make sure to add the end of line operator to make it OS Agnostic.
    $str = implode("\t",$data) . PHP_EOL;
    //wite the line to the file.
    fwrite($file_handle,$str);
    //make sure to close the file
    fclose($file_handle);
}

//as we aren't using PHP versions over 8.1, let's make our own string contains
if(!function_exists('str_contains')) {
    /**
     * str_contains is a newer PHP 8.1 function that I have adapted to suit the older 5.4 version of PHP on Mercury.
     * @param $haystack
     * @param $needle
     * @return bool
     */
    function str_contains($haystack, $needle)
    {
        return $needle !== '' && (stripos($haystack, $needle) !== false);
    }
}

function jobSearch($job, $string){
    //if this matches the job ID add it to the results
    if(str_contains($job[0],  $string)) return true;

    //if it matches the title then add it to the results
    if(str_contains($job[1], $string)) return true;

    //finally if it matches the description then add it to the results
    if(str_contains($job[2], $string)) return true;

    //return false if none of these match.
    return false;
}

/**
 * doJobSearch does a search for the entry in the file of job listings.
 * @param $file
 * @param $string
 * @param $filters
 * @return array
 */
function doJobSearch($file, $string, $filters = null){
    //our job list
    $joblist = readTabDelimitedFile($file);
    //initialise empty array for our results.
    $searchResults = [];

    //let's make sure we can do exact matches.
    $string = trim($string);

    //loop through our job list
    foreach($joblist as $jobID => $job){
        //Initialise an empty array for our true/false switches.
        $include = [];

        //this is where we check for filters...
        if($filters !== null){
            if(array_key_exists('application', $filters))
                switch($filters['application']){
                    case "Post":
                        $include['application'] = (trim($job[6]) == "on") && (jobSearch($job, $string));
                        break;
                    case "E-Mail":
                        $include['application'] = (trim($job[7]) == "on") && (jobSearch($job, $string));
                        break;
                }

            //filter by location
            if(array_key_exists('location', $filters))
                $include['location'] = (trim($job[8]) == $filters['location']) && (jobSearch($job, $string));

            //filter by position
            if(array_key_exists('position', $filters))
                $include['position'] = (trim($job[4]) == $filters['position']) && (jobSearch($job, $string));

            //filter by contract
            if(array_key_exists('contract', $filters))
                $include['contract'] = (trim($job[5]) == $filters['contract']) && (jobSearch($job, $string));
        }else
            $include['no_filter'] = (jobSearch($job, $string));

        //we need to check if any of these are false...
        $doNotInclude = true;
        foreach($include as $i)
            if(!$i) $doNotInclude = false;

        //one more thing to check...
        $date =  DateTime::createFromFormat('d/m/y', $job[3]);
        $now =  DateTime::createFromFormat('d/m/y', date('d/m/y'));

        //we don't want entries that have closed.
        if(($date < $now))
            $doNotInclude = false;

        //if there was no reason to remove the results, let's add them for return
        if($doNotInclude) $searchResults[$jobID] = $job;
    }

    //sort by furthest date
    usort($searchResults, function($a, $b){
        //index 3 is date
       return $b[3] - $a[3];
    });

    //return any results we have.
    return $searchResults;
}