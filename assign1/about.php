<?php
/*
    Post Job Form PHP
    Version 1
    Written by Mitchell Reynolds (1793098)
*/

//templated page constant.
const __PAGE__ = 'Assignment 2 - About Page';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
        <div class="row">
            <div class="col-8 offset-2">
                <h2 class="mb-5">Job Vacancy Posting System - About Page</h2>

                <h3 class="mb-1">What is the PHP version installed on Mercury?</h3>
                <p class="lead">The current version is (at the time of writing): 5.4.16</p>

                <h3 class="mb-1">What tasks have you not attempted or completed?</h3>
                <p class="lead">I have only one task that I haven't completed in the unit so far which is Task 2 (part 3) - I had completely forgotten this part.</p>

                <h3 class="mb-1">What special features have you done, or attempted, in creating the site that we should know about?</h3>
                <ul class="mb-5">
                    <li>I have created this site with the Bootstrap 5 CSS/SASS framework</li>
                    <li>I have used a functions.php file to reduce code pollution and allow for code reuse (making the code smaller and easier to manage)</li>
                    <li>I have implemented some new-age functionality in regard to inline-sanitization of form inputs (use of `filter_input()`)</li>
                    <li>The use of HTML5 validation features on the forms as well as validation through regex in PHP (mitigating bypass attacks)</li>
                    <li>(personal) I have also been using JetBrains' PHPStorm IDE</li>
                    <li>All of my labs and assignments are in <a target="_blank" href="https://gitlab.com/R3DAu/cos30020-awd">GitLab</a></li>
                </ul>

                <h3 class="mb-3 mt-2">What discussion points did you participate in on the unit's discussion board?</h3>
                <div class="row">
                    <div class="col-8 offset-2">
                        <img class="img-fluid " src="assets/img/Discussion.png" alt="discussion-board-topic"/>
                    </div>
                </div>
                <p class="lead">As I am pretty early for my assignment, I was the only to have posted at this point. The question was surrounding the Bootstrap implementation and if it was allowed.</p>

                <div class="row">
                    <div class="col-12 mt-4">
                        <p class="text-muted text-center"><a href="index.php">Return to home page</a>&nbsp;&nbsp;<a href="searchjobform.php">Return to search</a>&nbsp;&nbsp;<a href="postjobform.php">Post new vacancy</a></p>
                    </div>
                </div>
            </div>
    </main>
</div>
</body>
</html>