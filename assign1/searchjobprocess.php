<?php
/*
    Search Job Form PHP
    Version 1
    Written by Mitchell Reynolds (1793098)
*/

//templated page constant.
const __PAGE__ = 'Assignment 2 - Search Form';
//pull in the functions
require __DIR__ . '/functions.php';

//the relative path to the data directory to this script directory.
$data_dir = checkDir(__DIR__ . '/../../data/jobposts/');
$file = "$data_dir/jobs.txt";

//now we know the data directory exists, let's make sure our jobs file is in there.
createFile($file);

//get the search parameter
$search = filter_input(INPUT_GET, "search");

//get any of the filters if need be
$filters = [];

$application = filter_input(INPUT_GET, "application");
$location    = filter_input(INPUT_GET, "location");
$position    = filter_input(INPUT_GET, "position");
$contract    = filter_input(INPUT_GET, "contract");

if(!empty( $application ))
    $filters['application'] = $application;
if(!empty($location))
    $filters['location']    = $location;
if(!empty($position))
    $filters['position']    = $position;
if(!empty($contract))
    $filters['contract']    = $contract;

if(empty($filters)) $filters = null;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
        <div class="row">
            <div class="col-10 offset-1">
                <h2 class="mb-3">Job Vacancy Posting System</h2>
                <?php
                    if(empty($search)){
                ?>
                <div class="alert alert-danger">The search query was invalid, please try again below:</div>
                <form action="searchjobprocess.php" method="get">
                    <div class="input-group mb-1">
                        <label class="input-group-text" for="search">Search:</label>
                        <input class="form-control" type="search" name="search" id="search" placeholder="Search Here..."/>
                        <input class="btn btn-success" type="submit" value="Go" >
                    </div>
                </form>
                <?php }else{ ?>
                <h3 class="mb-3">Search Results</h3>
                <div class="row">

                        <?php
                            //let's do a search with filters if there's any set...
                            $searchResults = doJobSearch($file, $search, $filters);

                            if(!empty($searchResults)){
                                foreach($searchResults as $result){
                                    echo "<table class='table table-responsive'>
                                        <tbody>
                                        <tr class='table-dark'>
                                            <th scope='col'>Job ID</th>
                                            <th scope='col'>Title</th>
                                            <th scope='col'>Closing Date</th>
                                            <th scope='col'>Position</th>
                                            <th scope='col'>Application By</th>
                                            <th scope='col'>Location</th>
                                        </tr>
                                        <tr class='table-info'><td>$result[0]</td><td>$result[1]</td><td>$result[3]</td><td>$result[5] - $result[4]</td><td>".
                                        (($result[6] == "on")?"Post":"") . (($result[7] == "on")? (($result[6] == "on")?", ":"") . "Mail":"")
                                        ."</td><td>$result[8]</td></tr>
                                        <tr class='table-secondary'><th colspan='100'>Description</th></tr>
                                        <tr class='table-info'><td colspan='100'>$result[2]</td></tr>
                                         </tbody>
                                    </table>";
                                }
                            }else{
                                echo "<table class='table table-responsive'>
                                        <tbody>
                                        <tr class='table-dark'>
                                            <th scope='col'>Job ID</th>
                                            <th scope='col'>Title</th>
                                            <th scope='col'>Closing Date</th>
                                            <th scope='col'>Position</th>
                                            <th scope='col'>Application By</th>
                                            <th scope='col'>Location</th>
                                        </tr><tr><td colspan='100'><div class='mt-3 alert alert-warning'>Your search for '".$search."' has no results.</div></td></tr></tbody>
                                    </table>";
                            }
                        } ?>

                </div>
                <div class="row">
                    <div class="col-12 mt-4">
                        <p class="text-muted text-center"><a href="index.php">Return to home page</a>&nbsp;&nbsp;<a href="searchjobform.php">Return to search</a>&nbsp;&nbsp;<a href="postjobform.php">Post new vacancy</a></p>
                    </div>
                </div>
            </div>
    </main>
</div>
</body>
</html>
