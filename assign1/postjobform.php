<?php
/*
    Post Job Form PHP
    Version 1
    Written by Mitchell Reynolds (1793098)
*/

//templated page constant.
const __PAGE__ = 'Assignment 2 - Post Form';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
        <div class="row">
            <div class="col-8 offset-2">
                <h2 class="mb-3">Job Vacancy Posting System</h2>

                <form action="postjobprocess.php" method="post">
                    <div class="form-group mb-1">
                        <label for="positionID">Position ID</label>
                        <input class="form-control" type="text" pattern="P\d{4}" name="positionID" name="positionID" placeholder="P0000" required/>
                    </div>
                    <div class="form-group mb-1">
                        <label for="title">Title</label>
                        <input class="form-control" type="text" pattern="^[A-Za-z,! .]{1,20}$" name="title" name="title" placeholder="IT Manager" required/>
                    </div>
                    <div class="form-group mb-1">
                        <label for="description">Description</label>
                        <textarea class="form-control" pattern=".{1,260}" maxlength="260" name="description" id="description" rows="7" required> </textarea>
                    </div>
                    <div class="form-group mb-1">
                        <label for="closing-date">Closing Date</label>
                        <input class="form-control" type="text" pattern="^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[0-2])\/\d{2}$" name="closing-date" name="closing-date" placeholder="01/01/70" value="<?=date('d/m/y')?>"required/>
                    </div>
                    <div class="form-group mt-2">
                        <label>Position</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="position" id="position1" value="Full Time" required>
                        <label class="form-check-label" for="position1">Full Time</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="position" id="position2" value="Part Time" required>
                        <label class="form-check-label" for="position2">Part Time</label>
                    </div>

                    <div class="form-group mt-2">
                        <label>Contract</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="contract" id="contract1" value="On-Going" required>
                        <label class="form-check-label" for="contract1">On-Going</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="contract" id="contract2" value="Fixed Term" required>
                        <label class="form-check-label" for="contract2">Fixed Term</label>
                    </div>

                    <div class="form-group mt-2">
                        <label>Application by</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="appby_post" id="appby_post">
                        <label class="form-check-label" for="appby_post">Post</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" name="appby_mail" id="appby_mail">
                        <label class="form-check-label" for="appby_mail">E-Mail</label>
                    </div>

                    <div class="form-group mt-2">
                        <label for="location">Location</label>
                        <select class="form-select" id="location" name="location" required>
                            <option selected disabled>---</option>
                            <option value="ACT">ACT</option>
                            <option value="NSW">NSW</option>
                            <option value="NT">NT</option>
                            <option value="QLD">QLD</option>
                            <option value="SA">SA</option>
                            <option value="TAS">TAS</option>
                            <option value="VIC">VIC</option>
                            <option value="WA">WA</option>
                        </select>
                    </div>

                    <div class="row">
                        <div class="col-2 mt-2 ms-3 me-0 p-0">
                            <input class="btn btn-primary" style="width:100%" type="submit" value="Post">
                        </div>
                        <div class="col-2 mt-2 ms-1 p-0">
                            <input class="btn btn-danger" style="width:100%" type="reset" value="Reset">
                        </div>
                        <div class="col-12 mt-4">
                            <p class="text-muted">All fields are required. <a href="index.php">Return to Home Page</a></p>
                        </div>
                    </div>
                </form>
            </div>
    </main>
</div>
</body>
</html>