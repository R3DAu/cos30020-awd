<?php const __PAGE__ = 'Assignment 2 - Home Page'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <link href="assets/css/index.css" rel="stylesheet" />
    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
        <div class="row">
            <h1 class="mb-3"><?=__PAGE__?></h1>
            <hr />
            <p class="mb-5">Page by Mitchell Reynolds (1793098) - 1793098@student.swin.edu.au</p>

            <p>I declare that this assignment is my individual work. I have not worked  collaboratively nor have I copied from any other student’s work or from any other source.</p>
        </div>

        <div class="row">
            <div class="col-6">
                <ul>
                    <li><a href="postjobform.php">Post a job vacancy</a></li>
                    <li><a href="searchjobform.php">Search for a job vacancy</a></li>
                </ul>
            </div>
            <div class="col-6">
                <ul>
                    <li><a href="about.php">About this assignment</a></li>
                </ul>
            </div>
        </div>


    </main>
</div>
</body>
</html>