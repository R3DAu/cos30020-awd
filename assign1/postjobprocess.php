<?php
/*
    Post Job Process PHP
    Version 1
    Written by Mitchell Reynolds (1793098)
*/

//templated page constant.
const __PAGE__ = 'Assignment 2 - Post Process';

//pull in the functions
require __DIR__ . '/functions.php';

//the relative path to the data directory to this script directory.
$data_dir = checkDir(__DIR__ . '/../../data/jobposts/');
$file = "$data_dir/jobs.txt";

//now we know the data directory exists, let's make sure our jobs file is in there.
createFile($file);

//lets grab the contents of the file
$contents = readTabDelimitedFile($file);


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?=__PAGE__?></title>
        <meta charset="utf-8">
        <meta name="description" content="Web development">
        <meta name="keywords" content="HTML, CSS, JavaScript">
        <meta name="author" content="Mitchell Reynolds">

        <!-- Bootstrap CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
        <!-- Included Page Styles -->
        <style></style>
    </head>

<?php
//now let's handle the $_POST data side of things.
if($_SERVER['REQUEST_METHOD'] === 'POST'){
    //let's keep track of the errors here, so we can display them later on...
    $errors = [];

    //filter_input is a relatively new function, it allows the sanitization of input variables from GET,POST etc.
    //now we need to store the variables temporarily and securely, while we're here - let's remove whitespace.
    $positionID     = trim(filter_input(INPUT_POST, "positionID"));
    $title          = trim(filter_input(INPUT_POST, "title"));
    $description    = trim(filter_input(INPUT_POST, "description"));
    $closingDate    = trim(filter_input(INPUT_POST, "closing-date"));
    $position       = trim(filter_input(INPUT_POST, "position"));
    $contract       = trim(filter_input(INPUT_POST, "contract"));
    $appby_post     = trim(filter_input(INPUT_POST, "appby_post"));
    $appby_mail     = trim(filter_input(INPUT_POST, "appby_mail"));
    $location       = trim(filter_input(INPUT_POST, "location"));

    //now that we have all the fields, let's revalidate them (in case someone posted the form directly to be malicious or to get around the restrictions)
    $pid_match = preg_match('/P\d{4}/',$positionID);
    if(!$pid_match) $errors[] = "The position ID doesn't match the requested format of 'Pxxxx' where x is an integer.";

    //while we are here, we can check for the PID match.
    if(array_key_exists($positionID,$contents)) $errors[] = "That position ID ($positionID) already exists!";

    $title_match = preg_match('/^[A-Za-z,! .]{1,20}$/',$title);
    if(!$title_match) $errors[] = "The title must be (A-Z a-z space ! , or .) characters only and have a max length of 20";

    if(empty($description) || strlen($description) > 260) $errors[] = "You must fill out the description and it cannot be longer than 260 characters.";

    $closing_date_match = preg_match('/^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[0-2])\/\d{2}$/',$closingDate);
    if(!$closing_date_match) $errors[] = "The date must be in the format of dd/mm/yy";

    if(!$position) $errors[] = "One position type must be selected.";
    if(!$contract) $errors[] = "One contract type must be selected.";
    if(!$location) $errors[] = "One location must be selected.";
    if(!$appby_mail && !$appby_post) $errors[] = "At least one application by must be checked.";

    //now let's attach this to an array, so it's a bit nicer to handle.
    $data = array(
      $positionID, $title, $description, $closingDate, $position, $contract, $appby_post, $appby_mail, $location
    );
?>
    <body>
    <div class="col-lg-8 mx-auto p-3 py-md-5">
        <main>
            <div class="row">
                <div class="col-8 offset-2">
                    <?php
                        if(!empty($errors)){
                            echo "<div class='alert alert-danger'><h3>Errors detected</h3></div>";
                            echo "<ul>";
                            foreach($errors as $error)
                                echo "<li>$error</li>";
                            echo "</ul>";
                        }else{
                            echo "<div class='alert alert-success'><h3>You have successfully posted!</h3></div>";
                            //this is where we now need to write this information to file.
                            writeTabDelimitedFile($file, $data);
                        }
                    ?>
                </div>
                <div class="col-8 offset-2 text-center">
                    <a href="index.php">Back to home</a>
                </div>
            </div>
        </main>
    </div>
    </body>
<?php

}else{
    //return back to the form...
    header('location: postjobform.php');
}//end the if $_SERVER['request_method'] else statement.

?>
</html>
