<?php
/*
    Search Job Form PHP
    Version 1
    Written by Mitchell Reynolds (1793098)
*/

//templated page constant.
const __PAGE__ = 'Assignment 2 - Search Form';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
        <div class="row">
            <div class="col-8 offset-2">
                <h2 class="mb-3">Job Vacancy Posting System</h2>

                <form action="searchjobprocess.php" method="get">
                    <div class="input-group mb-2">
                        <label class="input-group-text" for="search">Search:</label>
                        <input class="form-control" type="search" name="search" name="search" placeholder="Search Here..."/>
                        <input class="btn btn-success" type="submit" value="Go" >
                    </div>
                    <label class="ms-4">Filter Options:</label>
                    <div class="input-group mb-1 px-4">
                        <select class="form-select" style="border-radius:0" name="application">
                            <option value="" selected>Application:</option>
                            <option value="Post">Post</option>
                            <option value="E-Mail">E-Mail</option>
                        </select>
                        <select class="form-select" style="border-radius:0" name="location">
                            <option value="" selected>Location:</option>
                            <option value="ACT">ACT</option>
                            <option value="NSW">NSW</option>
                            <option value="NT">NT</option>
                            <option value="QLD">QLD</option>
                            <option value="SA">SA</option>
                            <option value="TAS">TAS</option>
                            <option value="VIC">VIC</option>
                            <option value="WA">WA</option>
                        </select>
                        <select class="form-select" style="border-radius:0" name="position">
                            <option value="" selected>Position:</option>
                            <option value="Full Time">Full Time</option>
                            <option value="Part Time">Part Time</option>
                        </select>
                        <select class="form-select" style="border-radius:0" name="contract">
                            <option value="" selected>Contract:</option>
                            <option value="On-Going">On-Going</option>
                            <option value="Fixed Term">Fixed Term</option>
                        </select>
                    </div>

                    <div class="row">
                        <div class="col-12 mt-4">
                            <p class="text-muted"><a href="index.php">Return to Home Page</a></p>
                        </div>
                    </div>
                </form>
            </div>
    </main>
</div>
</body>
</html>