<?php
/*
    Math Functions PHP
    Version 1
    Written by Mitchell Reynolds (1793098)

    This file includes all the math functions required for the lab.
*/


/**
 * Factorial Function
 * Calculates the factorial value of positive numbers.
 * @param $n
 * @return float|int
 */
function factorial($n){
    //default result variable
    $result = 1;
    //default factor variable
    $factor = $n;

    //loop to multiply the factors until 1
    while($factor > 1){
        $result = $result * $factor;
        $factor--; //(Factor 1 is not multiplied)
    }

    //return the calculated value.
    return $result;
}

?>
