<?php
/*
    Leap Year Functions PHP
    Version 1
    Written by Mitchell Reynolds (1793098)

    This file includes all the leap year functions required for the lab.
*/

const __PAGE__ = 'Leap Year';

function is_leapyear($y){
    $year = $y;

    //if the year is divisible by 4 then it is a leap year.
    if(($year % 4) === 0 )
        //let's check if it's divisible by 100.
        if(($year % 100) === 0)
            //if the year is divisible by 400 and 100 it's a leap year.
            if(($year % 400) === 0) return true;
            else return false;
        else return true; //this means it's not divisible by 100

    //return false if all else.
    return false;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- rebase the URLs to here... -->
    <base href="/cos30020/s1793098/">
    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <!-- Included Page Styles -->
    <style>
        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
        <div class="col-6 offset-3">
            <h1><?=__PAGE__?></h1>
            <div>
                <?php
                if(isset($_POST['year'])){
                    $year = filter_input(INPUT_POST, "year");
                    if(is_leapyear($year)){
                        echo '<p class="text-bg-success px-2">
                                '.$year.' is a leapyear. <a class="float-end text-light" href="lab03/leapyearform.php">Go back.</a>
                              </p>';
                    }else{
                        echo '<p class="text-bg-danger text-light px-2">
                                '.$year.' is <ub><u>NOT</u></b> a leapyear. <a class="float-end text-light" href="lab03/leapyearform.php">Go back.</a>
                              </p>';
                    }

                }else {
                    echo '<p class="text-bg-danger text-light px-2">
                            Please enter a year through the form. <a class="float-end text-light" href="lab03/leapyearform.php">Go back.</a>
                          </p>';
                }
                ?>
            </div>
        </div>
    </main>
</div>
</body>
</html>
