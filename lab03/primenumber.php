<?php
const __PAGE__ = 'Prime Number';

/*
    Prime Number PHP
    Version 1
    Written by Mitchell Reynolds (1793098)

    This file includes prime number functions and view required for the lab.
*/


/**
 * Is Prime function
 * Checks if a number is prime
 * @param $n
 * @return bool
 */
function is_prime($n){
    //research completed with https://www.geeksforgeeks.org/php-check-number-prime/ to speed up the completion time of this task.

    $number = $n;
    //1 can't be a prime number.
    if($number === 1) return false;

    //cycle through the divisions until we hit the limit.
    for($i = 2; $i <= ($number/2); $i++)
        //if the number has no remainder then return false
        if(($number % $i) === 0) return false;

    return true;
}

 ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- rebase the URLs to here... -->
    <base href="/cos30020/s1793098/">
    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <div class="col-6 offset-3">
        <h1><?=__PAGE__?></h1>
        <div>
            <?php
            if(isset($_POST['prime'])){
                $prime = filter_input(INPUT_POST, "prime");
                if(is_prime($prime)){
                    echo '<p class="text-bg-success px-2">
                                '.$prime.' is a prime number. <a class="float-end text-light" href="lab03/primenumberform.php">Go back.</a>
                              </p>';
                }else{
                    echo '<p class="text-bg-danger text-light px-2">
                                '.$prime.' is <ub><u>NOT</u></b> a prime number. <a class="float-end text-light" href="lab03/primenumberform.php">Go back.</a>
                              </p>';
                }

            }else {
                echo '<p class="text-bg-danger text-light px-2">
                            Please enter integer through the form. <a class="float-end text-light" href="lab03/primenumberform.php">Go back.</a>
                          </p>';
            }
            ?>
        </div>
    </div>
</div>
</body>
</html>