<?php const __PAGE__ = 'Leap Year Form'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- rebase the URLs to here... -->
    <base href="/cos30020/s1793098/">
    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
        <div class="col-6 offset-3">
            <h1><?=__PAGE__?></h1>
            <form action="lab03/leapyear.php" method="post">
                <div class="mb-3">
                    <label for="year" class="form-label">Year</label>
                    <input type="number" class="form-control" name="year" id="year" min="1" max="9999" placeholder="1970">
                </div>
                <div class="mb-3">
                    <input type="submit" class="btn btn-primary" value="Submit">
                </div>
            </form>
        </div>
    </main>
</div>
</body>
</html>