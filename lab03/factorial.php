<?php const __PAGE__ = 'Factorial'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- rebase the URLs to here... -->
    <base href="/cos30020/s1793098/">
    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <!-- Included Page Styles -->
    <style>
        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
</head>
<body>
    <div class="col-lg-8 mx-auto p-3 py-md-5">
        <main>
            <?php
                include('mathfunctions.php');
            ?>
            <div class="col-6 offset-3">
                <h1><?=__PAGE__?></h1>
                <div>
                    <?php
                        if(isset($_POST['factor'])){
                            $num = filter_input(INPUT_POST, "factor");
                            if($num > 0){
                                if($num == round($num)) {
                                    echo '<p class="text-bg-success px-2">
                                            '.$num.'! is '.factorial($num).'. <a class="float-end text-light" href="lab03/factorialform.php">Go back.</a>      
                                          </p>';
                                }else{
                                    echo '<p class="text-bg-danger text-light px-2">
                                            Please enter a positive number. <a class="float-end text-light" href="lab03/factorialform.php">Go back.</a>      
                                          </p>';
                                }
                            }else{
                                echo '<p class="text-bg-danger text-light px-2">
                                        Please enter a positive number. <a class="float-end text-light" href="lab03/factorialform.php">Go back.</a>      
                                      </p>';
                            }
                        }else {
                            echo '<p class="text-bg-danger text-light px-2">
                                    Please enter the factor through the form. <a class="float-end text-light" href="lab03/factorialform.php">Go back.</a>      
                                  </p>';
                        }
                    ?>
                </div>
            </div>
        </main>
    </div>
</body>
</html>