DROP TABLE IF EXISTS `cars`;
CREATE TABLE `cars` (
  `car_id` int(11) NOT NULL,
  `make` varchar(25) NOT NULL,
  `model` varchar(40) NOT NULL,
  `price` float NOT NULL,
  `yom` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `cars` (`car_id`, `make`, `model`, `price`, `yom`) VALUES
(1,'Holden',	'Astra', 		14000.00,	2005),
(2,'BMW',	'X3',			35000.00,	2004),
(3,'Ford',	'Falcon',		39000.00,	2011),
(4,'Toyota',	'Corolla',		20000.00,	2012),
(5,'Holden',	'Commodore',	13500.00,	2005),
(6,'Holden',	'Astra', 		8000.00	,	2001),
(7,'Holden',	'Commodore',	28000.00,	2009),
(8,'Ford',	'Falcon',		14000.00,	2007),
(9,'Ford',	'Falcon',		7000.00	,	2003),
(10,'Ford',	'Laser',		10000.00,	2010),
(11,'Mazda',	'RX-7',			26000.00,	2000),
(12,'Toyota','Corolla',		12000.00,	2001),
(13,'Mazda',	'3',			14500.00,	2009);

SELECT * FROM cars;

SELECT make,model,price FROM cars ORDER BY make,model;

SELECT make,model FROM cars WHERE price >= 20000;

SELECT make,model FROM cars WHERE price < 15000;

SELECT make, AVG(price) FROM cars GROUP BY make;