<?php
//start the session
session_start();
//simple page constant
const __PAGE__ = 'Lab09 - Guessing Game';

//set the default.
if(!isset($_SESSION['number']))
    $_SESSION['number'] = rand(0,100);

//set the default.
if(!isset($_SESSION['guesses']))
    $_SESSION['guesses'] = 5;

//write this number to a variable.
$num = $_SESSION['number'];
$guesses = $_SESSION['guesses'];
$guess = null;

if(filter_input(INPUT_SERVER, "REQUEST_METHOD") == "POST"){
    if($guesses > 0) {
        $guesses--;
        $_SESSION['guesses'] = $guesses;
        $guess = filter_input(INPUT_POST, 'guess');
    }else{
        $_SESSION['guesses'] = 0;
        $guesses = 0;
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- rebase the URLs to here... -->
    <base href="/cos30020/s1793098/">
    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
        <h1><?=__PAGE__?></h1>
        <div class="alert alert-warning text-center p-0 m-0 mb-1"><p class="text-muted p-0 m-0">You have <?=$guesses?> guesses remaining</p></div>
        <?php
            if(!empty($guess))
                if($guess == $num){
                    $guesses = 6;
                    echo '<div class="alert alert-success text-center p-0 m-0 mb-1"><p class="text-muted p-0 m-0">That guess was correct!</p></div>';
                }else{
                    echo '<div class="alert alert-danger text-center p-0 m-0 mb-1"><p class="text-muted p-0 m-0">That guess was incorrect.</p></div>';
                }

            if($guesses <= 0){
                echo '<div class="alert alert-info text-center p-0 m-0 mb-1"><p class="text-muted p-0 m-0">The number was: '.$num.'</p></div>';
            }
        ?>

        <form action="lab09/guessinggame.php" method="post">
            <div class="input-group mb-2">
                <label class="input-group-text" for="guess">Guess a number between 0 and 100.</label>
                <input class="form-control" type="number" min="0" max="100" required name="guess" placeholder="<?=(isset($_GET['debug']))?$num:"0"?>" <?=($guesses <= 0|| $guess >= 6)?"readonly":""?>/>
                <input type="submit" class="btn btn-primary" value="submit" <?=($guesses <= 0 || $guess >= 6)?"disabled":""?>/>
            </div>
        </form>

        <p class="text-muted text-center"><a href="lab09/giveup.php">Give Up</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="lab09/startover.php">Start Over</a></p>
    </main>
</div>
</body>
</html>