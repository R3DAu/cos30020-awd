<?php
//start the session
session_start();

//set the default.
if(!isset($_SESSION['number']))
    $_SESSION['number'] = 0;

$num = $_SESSION['number'];
$num++;

//just increment the number and reset.
$_SESSION['number'] = $num;

header("location: number.php");