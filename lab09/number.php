<?php
    //start the session
    session_start();
    //simple page constant
    const __PAGE__ = 'Lab09 - Playing with numbers';

    //set the default.
    if(!isset($_SESSION['number']))
        $_SESSION['number'] = 0;

    //write this number to a variable.
    $num = $_SESSION['number'];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- rebase the URLs to here... -->
    <base href="/cos30020/s1793098/">
    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
        <h1><?=__PAGE__?></h1>
        <div class="alert alert-info text-center p-0 m-0 mb-1"><p class="text-muted p-0 m-0">The number is: <?=$num?></p></div>
        <p class="text-muted text-center"><a href="lab09/numberup.php">Number Up</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="lab09/numberdown.php">Number Down</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="lab09/numberreset.php">Number Reset</a></p>
    </main>
</div>
</body>
</html>