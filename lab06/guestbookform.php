<?php const __PAGE__ = 'Lab06 - Arrays (Guestbook)'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- Bootstrap CSS -->
    <link href="/cos30020/s1793098/assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">

    <main>
        <div class="row">
            <div class="col-8 offset-2">
                <h2><?=__PAGE__?></h2>
                <form action="guestbooksave.php" method="post">
                    <?php
                        if(filter_input(INPUT_GET, 'err') === "save")
                            echo '<div class="alert alert-danger">Unable to save directly, please use the form below</div>';
                     ?>
                    <div class="form-group mb-1">
                        <label for="name">Name</label>
                        <input class="form-control" type="text" name="name" id="name" placeholder="James Doe" required>
                    </div>
                    <div class="form-group mb-1">
                        <label for="email">E-Mail</label>
                        <input class="form-control" type="email" name="email" id="email" placeholder="james.doe@example.com" required>
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary mx-1 float-end" type="submit" value="Submit">
                        <input class="btn btn-danger mx-1 float-end" type="reset" value="Reset Form">
                    </div>
                </form>
                <a href="guestbookshow.php" class="btn btn-secondary">Show Guest Book</a>
            </div>
        </div>
    </main>
</div>
</body>
</html>