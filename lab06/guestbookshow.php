<?php

const __PAGE__ = 'Lab06 - Arrays (Guestbook)';

//the relative path to the data directory to this script directory.
$data_dir = __DIR__ . '/../../data/lab06';

//realpath returns the actual path, otherwise false if it doesn't exist.
//if the directory doesn't exist, let's create that.
if(!realpath($data_dir)) mkdir($data_dir, 0755, true);

//now we want to make sure that the data dir is now set to the actual path (just so permissions and syslinking isn't a thing.)
$data_dir = realpath($data_dir);
$file = "$data_dir/guestbook.txt";

//now we know the data directory exists, let's make sure our jobs file is in there.
if(!file_exists($file)) {
    //then we open, and instantly close the file for writing.
    $file_handle = fopen($file, 'w+') or die('Unable to open shop.txt for writing. Check file permissions!');
    fclose($file_handle);
}

$guest_list = [];

//open the file
$fh = fopen($file, "r");

//read line by line.
while(!feof($fh)){
    //trim whitespace before and after.
    $line = trim(fgets($fh));
    //ignore empty lines.
    if(empty($line)) continue;
    $aline = explode(',', $line);
    //we only want the name of item... not the quantity.
    $guest_list[$aline[0]] = $aline[1];
}

//make sure we close this file.
fclose($fh);

//sort the array key alphabetically
ksort($guest_list);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- Bootstrap CSS -->
    <link href="/cos30020/s1793098/assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">

    <main>
        <div class="row">
            <div class="col-8 offset-2">
                <h2><?=__PAGE__?></h2>
                <?php
                if(filter_input(INPUT_GET, 'success') === "true")
                    echo '<div class="alert alert-success">Your entry has been saved!</div>';

                echo "<table class='table'>
                        <thead class='thead-dark'>
                            <tr>
                                <th scope='col'>Name</th>
                                <th scope='col'>Email</th>
                            </tr>
                        </thead>
                        <tbody>";

                foreach($guest_list as $name => $email)
                    echo "<tr><td>".$name."</td><td>".$email."</td></tr>";


                echo "</tbody></table>";
                ?>
                <a href="guestbookform.php" class="btn btn-secondary">Add new entry</a>
            </div>
        </div>
    </main>
</div>
</body>
</html>