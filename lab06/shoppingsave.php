<?php
/*
    Shopping Save PHP
    Version 1
    Written by Mitchell Reynolds (1793098)
*/
const __PAGE__ = 'Lab06 - Arrays';

//the relative path to the data directory to this script directory.
$data_dir = __DIR__ . '/../../data/';

//realpath returns the actual path, otherwise false if it doesn't exist.
//if the directory doesn't exist, let's create that.
if(!realpath($data_dir)) mkdir($data_dir, 0755, true);

//now we want to make sure that the data dir is now set to the actual path (just so permissions and syslinking isn't a thing.)
$data_dir = realpath($data_dir);
$file = "$data_dir/shop.txt";

//now we know the data directory exists, let's make sure our jobs file is in there.
if(!file_exists($file)) {
    //then we open, and instantly close the file for writing.
    $file_handle = fopen($file, 'w+') or die('Unable to open shop.txt for writing. Check file permissions!');
    fclose($file_handle);
}

//quick and dirty function for writing to the file.
function WriteData($file, $item, $qty){
    //let's get ready to write the file.
    $handle = fopen($file, 'a');
    $data = "$item,$qty". PHP_EOL;
    fwrite($handle,$data);
    fclose($handle);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- rebase the URLs to here... -->
    <base href="/cos30020/s1793098/">
    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <!-- Included Page Styles -->
    <style></style>
</head>
    <body>
        <div class="col-lg-8 mx-auto p-3 py-md-5">
            <main>
                <div class="row">
                    <div class="col-8 offset-2">
                        <h1><?=__PAGE__?></h1>
                        <?php
                        if(filter_input(INPUT_SERVER, "REQUEST_METHOD") === "POST"){
                            $item = filter_input(INPUT_POST, "item");
                            $qty = filter_input(INPUT_POST, "qty");

                            //initialise array
                            $data = [];
                            $item_data = [];

                            //open the file
                            $fh = fopen($file, "r");

                            //read line by line.
                            while(!feof($fh)){
                                //trim whitespace before and after.
                                $line = trim(fgets($fh));
                                //ignore empty lines.
                                if(empty($line)) continue;
                                $aline = explode(',', $line);
                                $data[] = $aline;
                                //we only want the name of item... not the quantity.
                                $item_data[] = $aline[0];
                            }
                            //make sure we close this file.
                            fclose($fh);

                            $newdata = !(in_array($item, $item_data, false));

                            //because we created the file first, let's check if there is any data, then write to it if not.
                            if((count($data) === 0) && (count($item_data) === 0)) $newdata = true;

                            //we use the function WriteData here to clean up the code.
                            if($newdata){
                                WriteData($file, $item, $qty);
                                $data[] = array($item, $qty);
                                echo "<div class='alert alert-success'>Successfully added item and quantity to the list.</div>";
                            }else{
                                echo "<div class='alert alert-danger'>That item is already in the list</div>";
                            }

                            //sort the array
                            sort($data);

                            echo "<h2> Shopping List </h2>";

                            echo "<table class='table'>
                                    <thead class='thead-dark'>
                                        <tr>
                                            <th scope='col'>Item</th>
                                            <th scope='col'>Qty</th>
                                        </tr>
                                    </thead>
                                    <tbody>";
                                    foreach($data as $item){
                                        echo "<tr><td>".$item[0]."</td><td>".$item[1]."</td></tr>";
                                    }
                                echo "</tbody></table>";
                        }else
                            echo "<p>Please enter item and quantity in the input form</p>";
                        ?>
                    </div>
                </div>
            </main>
        </div>
    </body>
</html>

