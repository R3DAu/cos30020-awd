<?php

//the relative path to the data directory to this script directory.
$data_dir = __DIR__ . '/../../data/lab06';

//realpath returns the actual path, otherwise false if it doesn't exist.
//if the directory doesn't exist, let's create that.
if(!realpath($data_dir)) mkdir($data_dir, 0755, true);

//now we want to make sure that the data dir is now set to the actual path (just so permissions and syslinking isn't a thing.)
$data_dir = realpath($data_dir);
$file = "$data_dir/guestbook.txt";

//now we know the data directory exists, let's make sure our jobs file is in there.
if(!file_exists($file)) {
    //then we open, and instantly close the file for writing.
    $file_handle = fopen($file, 'w+') or die('Unable to open shop.txt for writing. Check file permissions!');
    fclose($file_handle);
}

//quick and dirty function for writing to the file.
function WriteData($file, $name, $email){
    //let's get ready to write the file.
    $handle = fopen($file, 'a');
    $data = "$name,$email". PHP_EOL;
    fwrite($handle,$data);
    fclose($handle);
}

if(filter_input(INPUT_SERVER, "REQUEST_METHOD") === "POST") {
    $name = trim(filter_input(INPUT_POST, "name"));
    $email = trim(filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL));

    if(empty($name) || empty($email))
        die("<h3>Unable to continue, all fields need to be filled out! Use the back button to continue.</h3>");

    if(!filter_var($email, FILTER_VALIDATE_EMAIL))
        die("<h3>Unable to continue, Email is not valid! Use the back button to correct this mistake.</h3>");

    //initialise empty arrays.
    $guests_name = [];
    $guests_email = [];

    //open the file
    $fh = fopen($file, "r");

    //read line by line.
    while(!feof($fh)){
        //trim whitespace before and after.
        $line = trim(fgets($fh));
        //ignore empty lines.
        if(empty($line)) continue;
        $aline = explode(',', $line);
        //we only want the name of item... not the quantity.
        $guests_name[] = $aline[0];
        $guests_email[] = $aline[1];
    }

    //make sure we close this file.
    fclose($fh);

    $newdata = !((in_array($name,$guests_name)) || (in_array($email, $guests_email)));

    if($newdata)
        WriteData($file,$name,$email);
    else
        die("<h3>Unable to continue, Duplicate name or username. Please go back and correct this.</h3>");

    //just redirect them back to the form...
    header("location: guestbookshow.php?success=true");
}else{
    //just redirect them back to the form...
    header("location: guestbookform.php?err=save");
}