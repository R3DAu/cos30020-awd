<?php
use Swinburne\database;

//templated page constant.
const __PAGE__ = 'Lab 08 - Add VIP Member';

//grab the settings file
require_once("settings.php");

//grab our database class.
require_once('database.php');

//grab any functions that are needed
require_once('functions.php');

//create a new instance of the database class.
$database = new database($settings);

//connect to the database.
$database->connect();

//let's see if the vipmembers table exists...
if(!$database->table_exists("vipmembers")){
    //if this doesn't exist
    //then we need to create... This calls the helper function (In functions.php)
    create_vip_table($database);
}

?>

    <!-- FORM GOES HERE -->

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <title><?=__PAGE__?></title>
        <meta charset="utf-8">
        <meta name="description" content="Web development">
        <meta name="keywords" content="HTML, CSS, JavaScript">
        <meta name="author" content="Mitchell Reynolds">

        <!-- rebase the URLs to here... -->
        <base href="/cos30020/s1793098/">
        <!-- Bootstrap CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
        <!-- Included Page Styles -->
        <style></style>
    </head>
    <body>
    <div class="col-lg-8 mx-auto p-3 py-md-5">
        <main>
            <div class="row">
                <div class="col-8 offset-2">
                    <h2 class="mb-3">Lab 08 - Add VIP Member form</h2>

<?php
//if we got this far, we can now proceed with the member form side of things...
if(filter_input(INPUT_SERVER, "REQUEST_METHOD") === "POST"){
    //we now need to grab the fields. Generally I would add the validation here however, I will skip that here due
    //to time constraints.

    $fname  = filter_input(INPUT_POST, "fname");
    $lname  = filter_input(INPUT_POST, "lname");
    $email  = filter_input(INPUT_POST, "email");
    $gender = filter_input(INPUT_POST, "gender");
    $phone  = filter_input(INPUT_POST, "phone");

    //let's work some magic and see if this member already exists...
    $sql = "SELECT * FROM vipmembers WHERE fname = ? AND lname = ? AND email = ?";
    $bind = array($fname, $lname, $email);

    $stmt = $database->con->prepare($sql);
    $stmt->execute($bind);
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if(count($results) > 0){
        //then we have a match.
        echo "<div class='alert alert-danger'>This member already exists.</div>";
    }else{
        $sql  = "INSERT INTO vipmembers (member_id, fname, lname, gender, email, phone) VALUES (null, ?, ?, ?, ?, ?)";
        $bind = array($fname, $lname, $gender, $email, $phone);

        $stmt = $database->con->prepare($sql);
        $stmt->execute($bind);
        echo "<div class='alert alert-success'>This member has been added successfully.</div>";
    }
}
?>



                <form action="lab08/member_add_form.php" method="post">
                    <div class="form-group mb-1">
                        <label for="fname">First Name</label>
                        <input class="form-control" type="text" <?=(!empty($fname)?"value='$fname'":"")?> pattern="^[A-Za-z-]{1,40}$" name="fname" name="fname" placeholder="Joe" required/>
                    </div>
                    <div class="form-group mb-1">
                        <label for="lname">Last Name</label>
                        <input class="form-control" type="text" <?=(!empty($lname)?"value='$lname'":"")?> pattern="^[A-Za-z-]{1,40}$" name="lname" name="lname" placeholder="Blogs" required/>
                    </div>
                    <div class="form-group mt-2">
                        <label for="gender">Gender</label>
                        <select class="form-select" id="gender" name="gender" required>
                            <option <?=(!empty($gender)?"":"selected")?> disabled>---</option>
                            <option <?=(($gender == "m")?"selected":"")?> value="m">Male</option>
                            <option <?=(($gender == "f")?"selected":"")?> value="f">Female</option>
                            <option <?=(($gender == "b")?"selected":"")?> value="b">Non-Binary</option>
                            <option <?=(($gender == "n")?"selected":"")?> value="n">Unspecified</option>
                            <option <?=(($gender == "o")?"selected":"")?> value="o">Other</option>
                        </select>
                    </div>
                    <div class="form-group mb-1">
                        <label for="email">Email</label>
                        <input class="form-control" <?=(!empty($email)?"value='$email'":"")?> type="email" name="email" name="email" placeholder="joe.blogs@something.com" required/>
                    </div>
                    <div class="form-group mb-1">
                        <label for="phone">Phone Number</label>
                        <input class="form-control" type="text" <?=(!empty($phone)?"value='$phone'":"")?> pattern="^(\+?\(61\)|\(\+?61\)|\+?61|\(0[1-9]\)|0[1-9])?( ?-?[0-9]){7,9}$" name="phone" name="phone" placeholder="+613 9000 1234" required/>
                    </div>

                    <div class="row">
                        <div class="col-2 mt-2 ms-3 me-0 p-0">
                            <input class="btn btn-primary" style="width:100%" type="submit" value="Post">
                        </div>
                        <div class="col-2 mt-2 ms-1 p-0">
                            <input class="btn btn-danger" style="width:100%" type="reset" value="Reset">
                        </div>
                        <div class="col-12 mt-4">
                            <p class="text-muted">All fields are required. <a href="lab08/vip_member.php">Return to Home Page</a></p>
                        </div>
                    </div>
                </form>
            </div>
    </main>
</div>
</body>
</html>
