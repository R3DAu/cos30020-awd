<?php
use Swinburne\database;

//grab the settings file
require_once("settings.php");

//grab our database class.
require_once('database.php');

//create a new instance of the database class.
$database = new database($settings);

//connect to the database.
$database->connect();

//let's make a query.
$query = $database->con->prepare("SELECT car_id, make, model, price FROM cars");
$query->execute();

$cars = $query->fetchAll(PDO::FETCH_ASSOC);
?>

<?php const __PAGE__ = 'Lab 08 - Displaying records'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- rebase the URLs to here... -->
    <base href="/cos30020/s1793098/">
    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
        <h1><?=__PAGE__?></h1>

        <table class="table table-striped">
            <thead>
                <th class="col">Car ID</th>
                <th class="col">Make</th>
                <th class="col">Model</th>
                <th class="col">Price</th>
            </thead>
            <tbody>
            <?php
                foreach($cars as $car)
                    echo "<tr><td>{$car['car_id']}</td><td>{$car['make']}</td><td>{$car['model']}</td><td>{$car['price']}</td></tr>";
            ?>
            </tbody>
        </table>
    </main>
</div>
</body>
</html>
