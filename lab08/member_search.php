<?php
use Swinburne\database;

//templated page constant.
const __PAGE__ = 'Lab 08 - Search For VIP Member';

//grab the settings file
require_once("settings.php");

//grab our database class.
require_once('database.php');

//grab any functions that are needed
require_once('functions.php');

//create a new instance of the database class.
$database = new database($settings);

//connect to the database.
$database->connect();
?>

    <!-- FORM GOES HERE -->

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <title><?=__PAGE__?></title>
        <meta charset="utf-8">
        <meta name="description" content="Web development">
        <meta name="keywords" content="HTML, CSS, JavaScript">
        <meta name="author" content="Mitchell Reynolds">

        <!-- rebase the URLs to here... -->
        <base href="/cos30020/s1793098/">
        <!-- Bootstrap CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
        <!-- Included Page Styles -->
        <style></style>
    </head>
    <body>
    <div class="col-lg-8 mx-auto p-3 py-md-5">
        <main>
            <div class="row">
                <div class="col-8 offset-2">
                    <h2 class="mb-3"><?=__PAGE__?></h2>

<?php
//if we got this far, we can now proceed with the member form side of things...
if(filter_input(INPUT_SERVER, "REQUEST_METHOD") === "GET" && !(empty($_GET['search']))){
    //we now need to grab the fields. Generally I would add the validation here however, I will skip that here due
    //to time constraints.

    $lname  = filter_input(INPUT_GET, "search");

    //let's work some magic and see if this member already exists...
    $sql = "SELECT * FROM vipmembers WHERE lname LIKE ?";
    $bind = array("%$lname%");

    $stmt = $database->con->prepare($sql);
    $stmt->execute($bind);
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Member ID</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Email</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($results as $result)
                    echo "<tr><td>{$result['member_id']}</td><td>{$result['fname']}</td><td>{$result['lname']}</td><td>{$result['email']}</td></tr>";
                ?>
                </tbody>
            </table>
            <div class="col-12 mt-4">
                <p class="text-muted"><a href="lab08/vip_member.php">Return to Home Page</a></p>
            </div>
        </main>
    </div>
    </body>
</html>

<?php }else{ ?>

        <form method="get">
            <div class="input-group mb-2">
                <label class="input-group-text" for="search">Last Name:</label>
                <input class="form-control" type="search" name="search" name="search" placeholder="Search Last Name..."/>
                <input class="btn btn-success" type="submit" value="Go" >
            </div>
        </form>
          <div class="col-12 mt-4">
            <p class="text-muted"><a href="lab08/vip_member.php">Return to Home Page</a></p>
        </div>
     </main>
    </div>
    </body>
</html>
<?php
}