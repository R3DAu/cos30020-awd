<?php
use Swinburne\database;

//templated page constant.
const __PAGE__ = 'Lab 08 - Display VIP Member';

//grab the settings file
require_once("settings.php");

//grab our database class.
require_once('database.php');

//grab any functions that are needed
require_once('functions.php');

//create a new instance of the database class.
$database = new database($settings);

//connect to the database.
$database->connect();

//Collect the results we require.
$stmt = $database->con->query("SELECT member_id, fname, lname FROM vipmembers");
$stmt->execute();
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- rebase the URLs to here... -->
    <base href="/cos30020/s1793098/">
    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
        <h1><?=__PAGE__?></h1>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Member ID</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                </tr>
            </thead>
            <tbody>
            <?php
                foreach($results as $result)
                    echo "<tr><td>{$result['member_id']}</td><td>{$result['fname']}</td><td>{$result['lname']}</td></tr>";
            ?>
            </tbody>
        </table>
        <div class="col-12 mt-4">
            <p class="text-muted"><a href="lab08/vip_member.php">Return to Home Page</a></p>
        </div>
    </main>
</div>
</body>
</html>