<?php const __PAGE__ = 'Lab 08 - VIP Home'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?=__PAGE__?></title>
    <meta charset="utf-8">
    <meta name="description" content="Web development">
    <meta name="keywords" content="HTML, CSS, JavaScript">
    <meta name="author" content="Mitchell Reynolds">

    <!-- rebase the URLs to here... -->
    <!--base href="/cos30020/s1793098/"-->
    <!-- Bootstrap CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <!-- Included Page Styles -->
    <style></style>
</head>
<body>
<div class="col-lg-8 mx-auto p-3 py-md-5">
    <main>
        <h1><?=__PAGE__?></h1>
        <div class="row">
            <div class="col-6">
                <ul>
                    <li><a href="member_add_form.php">Add new Member</a></li>
                    <li><a href="member_display.php">Display members</a></li>
                </ul>
            </div>
            <div class="col-6">
                <ul>
                    <li><a href="member_search.php">Search for a member</a></li>
                </ul>
            </div>
        </div>
    </main>
</div>
</body>
</html>