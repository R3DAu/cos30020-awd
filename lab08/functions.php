<?php
// this file contains some basic helper functions
function create_vip_table($database){
    $columns = array(
            "member_id INT AUTO_INCREMENT",
            "fname VARCHAR(40) NOT NULL",
            "lname VARCHAR(40) NOT NULL",
            "gender VARCHAR(1) NOT NULL",
            "email VARCHAR(40) NOT NULL",
            "phone VARCHAR(20) NOT NULL",
            "PRIMARY KEY (member_id)"
        );
    $database->table_create('vipmembers', $columns);
}